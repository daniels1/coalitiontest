<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use File;

class CoalitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $filename = 'products.json';

        $data = $request->all();

        if (File::exists($filename))
        {

            $products = json_decode(File::get($filename), true);

            $products[] = $data;

        }else{

            $products[0] = $data;

        }

        File::put(public_path($filename), json_encode($products));

        return  ['success'=> true, 'products' => json_decode(File::get($filename),0)];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function showAll()
    {

        $filename = 'products.json';

        if (File::exists($filename))
        {
        
            $data = json_decode(File::get($filename), 0);
        }    

        return ['data' => $data];

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {        

        $filename = 'products.json';

        $data = $request->all();

        if (File::exists($filename))
        {

            File::put(public_path($filename), json_encode($data));

        }

        return  ['success'=> true, 'products' => json_decode(File::get($filename),0)];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
