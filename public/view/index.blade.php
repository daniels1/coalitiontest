<!DOCTYPE html>
<html class=" ">
    <head>
        <!-- 
         * @Package: Ultra Admin - Responsive Theme
         * @Subpackage: Bootstrap
         * @Version: 4.1
         * This file is part of Ultra Admin Theme.
        -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>Coalition Skills Test</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon" />    <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="assets/images/apple-touch-icon-57-precomposed.png">	<!-- For iPhone --
>        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/apple-touch-icon-114-precomposed.png">    <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/apple-touch-icon-72-precomposed.png">    <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/apple-touch-icon-144-precomposed.png">    <!-- For iPad Retina display -->


        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular.js"></script>        
        <script src="assets/js/ng-alertify/ng-alertify.js"></script>

        <!-- CORE CSS FRAMEWORK - START -->
        <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css"/>
        <!-- CORE CSS FRAMEWORK - END -->

        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE CSS TEMPLATE - START -->
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css"/>
        <!-- CORE CSS TEMPLATE - END -->

    </head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
    <body ng-app="coalitionApp" ng-cloak><!-- START TOPBAR -->
        <div class='page-topbar '>
            <div class='logo-area'>

            </div>
            <div class='quick-area'>
                <div class='pull-left'>
                    <ul class="info-menu left-links list-inline list-unstyled">
                        <li class="sidebar-toggle-wrap">
                            <a href="#" data-toggle="sidebar" class="sidebar_toggle">
                                <i class="fa fa-bars"></i>
                            </a>
                        </li>
                    </ul>
                </div>		
                <div class='pull-right'>
                    <ul class="info-menu right-links list-inline list-unstyled">
                        <li class="profile">
                            <a href="#" data-toggle="dropdown" class="toggle">
                                <img src="data/profile/profile.jpg" alt="user-image" class="img-circle img-inline">
                                <span>Daniel Sánchez <i class="fa fa-angle-down"></i></span>
                            </a>                            
                        </li>
                    </ul>			
                </div>		
            </div>

        </div>
        <!-- END TOPBAR -->
        <!-- START CONTAINER -->
        <div class="page-container row-fluid">

            <!-- SIDEBAR - START -->
            <div class="page-sidebar ">

                <!-- MAIN MENU - START -->
                <div class="page-sidebar-wrapper" id="main-menu-wrapper"> 

                    <!-- USER INFO - START -->
                    <div class="profile-info row">

                        <div class="profile-image col-md-4 col-sm-4 col-xs-4">
                            <a href="ui-profile.html">
                                <img src="data/profile/profile.jpg" class="img-responsive img-circle">
                            </a>
                        </div>

                        <div class="profile-details col-md-8 col-sm-8 col-xs-8">

                            <h3>
                                <a href="ui-profile.html">Daniel Sánchez</a>

                                <!-- Available statuses: online, idle, busy, away and offline -->
                                <span class="profile-status online"></span>
                            </h3>

                            <p class="profile-title">Fullstack Developer</p>

                        </div>

                    </div>
                    <!-- USER INFO - END -->



                    <ul class='wraplist'>	


                        <li class=""> 
                            <a href="index.html">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>

                    </ul>

                </div>
                <!-- MAIN MENU - END -->



            </div>
            <!--  SIDEBAR - END -->
            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper" style=''>

                    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                        <div class="page-title">

                            <div class="pull-left">
                                <h1 class="title">Coalition Test</h1>                            
                            </div>                            

                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-lg-12" ng-controller="coalitionController as ct" ng-init="initLoad()">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title pull-left">Inventory</h2>
                                <div class="actions panel_actions pull-right">
                                    <i class="box_toggle fa fa-chevron-down"></i>
                                    <i class="box_close fa fa-times"></i>
                                </div>
                            </header>
                            <div class="content-body">    <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">                                 

                                        <!-- start -->

                                        <div class="row center-block">

                                            <form class="form-inline" ng-submit="saveProduct()" name="products_form">
                                              <input type="hidden" ng-model="product_id">
                                              <div class="form-group">
                                                <label for="product_name">Name</label>
                                                <input type="text" class="form-control" id="product_name" ng-model="product_name" required placeholder="Product name">
                                              </div>
                                              <div class="form-group">
                                                <label for="product_quantity">Quantity</label>
                                                <input type="text" class="form-control" id="product_quantity" ng-model="product_quantity" required placeholder="Quantity in stock">
                                              </div>
                                              <div class="form-group">
                                                <label for="product_price">Price</label>
                                                <input type="text" class="form-control" id="product_price" ng-model="product_price" required placeholder="Price per item">
                                              </div>
                                              
                                              <button type="submit" class="btn btn-primary">Save</button>
                                            </form>                            
                
                                        </div>

                                        <hr>

                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <h3>Order summary</h3><br>
                                                <div class="table-responsive">
                                                    <table class="table table-hover invoice-table">
                                                        <thead>
                                                            <tr>
                                                                <td><h4>Product name</h4></td>
                                                                <td class="text-center"><h4>Quantity in stock</h4></td>
                                                                <td class="text-center"><h4>Price per item</h4></td>
                                                                <td class="text-center"><h4>Datetime submitted</h4></td>
                                                                <td class="text-right"><h4>Total</h4></td>
                                                                <td></td>
                                                                <td></td>                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr ng-repeat="product in products track by $index">
                                                                <td><% product.product_name %></td>
                                                                <td class="text-center"><% product.product_quantity %></td>
                                                                <td class="text-center"><% product.product_price | currency:"$":2 %></td>
                                                                <td class="text-center"><% product.product_datetime | date:"MM/dd/yyyy h:mma" %></td>
                                                                <td class="text-center"><% (product.product_quantity * product.product_price) | currency:"$":2 %></td>
                                                                <td><a ng-click="editItem($index)"><i class="fa fa-edit"></i></a></td>
                                                                <td><a ng-click="delItem($index)"><i class="fa fa-trash"></i></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="no-line"></td>
                                                                <td class="no-line"></td>
                                                                <td class="no-line text-center"><h4>Total</h4></td>
                                                                <td class="no-line text-right"><h3 style='margin:0px;' class="text-primary"><% summary_total | currency:"$":2 %></h3></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </section></div>


                </section>
            </section>
            <!-- END CONTENT -->
            </div>
        <!-- END CONTAINER -->
        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


        <!-- CORE JS FRAMEWORK - START --> 
        <script src="assets/js/jquery-1.11.2.min.js" type="text/javascript"></script> 
        <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>  
        <script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <!-- CORE JS FRAMEWORK - END --> 

        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->         

        <script type="text/javascript">
            
            var app = angular.module("coalitionApp",['Alertify'], function($interpolateProvider) {
                $interpolateProvider.startSymbol('<%');
                $interpolateProvider.endSymbol('%>');
            });

            app.controller('coalitionController', ['$scope', '$http', 'Alertify', function($scope, $http, Alertify) {

                var ct = $scope;
                    ct.products = [];
                    ct.summary_total = 0;

                ct.initLoad = function(){

                    $http.get("api/all").then(function (response) {
             
                        ct.products = response.data.data
                        angular.forEach(ct.products, function(prod, key) {
                          ct.summary_total += (prod.product_quantity * prod.product_price);
                        })   

                    })

                }

                ct.saveProduct = function(){

                    if (Number.isInteger(ct.product_id) && ct.product_id !== "") {

                        ct.summary_total -= (ct.products[ct.product_id].product_quantity * ct.products[ct.product_id].product_price);

                        ct.products[ct.product_id].product_name = ct.product_name;
                        ct.products[ct.product_id].product_quantity = ct.product_quantity;
                        ct.products[ct.product_id].product_price = ct.product_price;

                        ct.summary_total += (ct.product_quantity * ct.product_price);

                        $http.put("api", ct.products)
                             .success(function(data) {
                                  if (data.success) { 
                                    ct.products = data.products
                                  }
                              });

                    }else {

                        var new_product = {
                            'product_name': ct.product_name,
                            'product_quantity': ct.product_quantity,
                            'product_price': ct.product_price,
                            'product_datetime': new Date()
                        }                        

                        $http.post("api", new_product)
                             .success(function(data) {
                                  if (data.success) { 
                                    ct.products = data.products
                                  }
                              });

                        ct.summary_total += (ct.product_quantity * ct.product_price);                        
                    }
                    
                    ct.product_id  = '';
                    ct.product_name = '';
                    ct.product_quantity = '';
                    ct.product_price = '';
                    ct.product_datetime = '';

                }

                ct.editItem = function(index){

                    ct.product_id = index;
                    ct.product_name = ct.products[index].product_name;
                    ct.product_quantity = ct.products[index].product_quantity;
                    ct.product_price = ct.products[index].product_price;
                    ct.product_datetime = ct.products[index].product_datetime;
                }

                ct.delItem = function(index){

                    ct.summary_total -= (ct.products[index].product_quantity * ct.products[index].product_price);

                    ct.products.splice(index, 1);
                    ct.product_id  = '';

                    $http.put("api", ct.products)
                             .success(function(data) {
                                  if (data.success) { 
                                    ct.products = data.products
                                  }
                              });

                    Alertify.success('Product Remove');

                }

                ct.clearAll = function(){

                    ct.products = [];
                    ct.summary_total = 0;

                }

                
            }]);
        </script>
        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE TEMPLATE JS - START --> 
        <script src="assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 

        <!-- Sidebar Graph - START --> 
        <script src="assets/plugins/sparkline-chart/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="assets/js/chart-sparkline.js" type="text/javascript"></script>
        <!-- Sidebar Graph - END --> 
        
    </body>
</html>



